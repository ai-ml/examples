## Example - Kubeflow Metadata

### What is it about?

Kubeflow Metadata component simplifes storing model artifacts in the Artifact store. <br/>
For more info check the links https://www.kubeflow.org/docs/components/pipelines/concepts/metadata/ and https://github.com/google/ml-metadata/blob/master/g3doc/get_started.md <br/>

### How to run?

- Navigate to **metadata/demo_metadata.ipynb**
- Run the cells
