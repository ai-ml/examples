### Example - PyTorch Job

## What is it about?

Distributed training with Tensorflow.
You can select level of parallelism, number of workers and resources for each worker (GPU, CPU).

## How to run?  

* In Terminal, run: `kubectl apply -f tfjob.yaml`.  
* Monitor tfjob with: `kubectl get tfjob`
