# Example Tensorboard

Demonstrate usage of Tensorboards on ml.cern.ch.   

### How to run?
- Create a Notebook Server with a Workspace Volume
- Open the Notebook Server and clone this repo
- Run the notebook mnist-kfp.ipynb
- Navigate to Tensorboards in the ml.cern.ch UI
- Create a new Tensorboard
    - Select PVC
    - PVC name should correspond to the Workspace Volume of the notebook
    - Mount path should be: `tb_logs` (relative to /home/jovyan)
- Monitor the training after clicking Connect
