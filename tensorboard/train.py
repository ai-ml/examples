import os
import numpy as np
import tensorflow as tf

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
  try:
    tf.config.set_logical_device_configuration(
        gpus[0],
        [tf.config.LogicalDeviceConfiguration(memory_limit=1024)])
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Virtual devices must be set before GPUs have been initialized
    print(e)


(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

# Rescale the images from [0,255] to the [0.0,1.0] range.
x_train, x_test = x_train[..., np.newaxis]/255.0, x_test[..., np.newaxis]/255.0

os.system('pwd')
os.system('ls -la')
os.system('whoami')

np.save('xtrain.npy', x_train)
np.save('ytrain.npy', y_train)
np.save('xtest.npy', x_test)
np.save('ytest.npy', y_test)


# Load data
x_train = np.load('xtrain.npy')
y_train = np.load('ytrain.npy')

x_test = np.load('xtest.npy')
y_test = np.load('ytest.npy')

# Filter 3 and 6
def filter_36(x, y):
    keep = (y == 3) | (y == 6)
    x, y = x[keep], y[keep]
    y = y == 3
    return x,y

print("Number of unfiltered training examples:", len(x_train))
print("Number of unfiltered test examples:", len(x_test))

x_train, y_train = filter_36(x_train, y_train)
x_test, y_test = filter_36(x_test, y_test)

print("Number of filtered training examples:", len(x_train))
print("Number of filtered test examples:", len(x_test))

# Save modified data
np.save('xtrain_filtered.npy', x_train)
np.save('ytrain_filtered.npy', y_train)

np.save('xtest_filtered.npy', x_test)
np.save('ytest_filtered.npy', y_test)

model = tf.keras.Sequential()
model.add(tf.keras.layers.Conv2D(32, [3, 3], activation='relu', input_shape=(28,28,1)))
model.add(tf.keras.layers.Conv2D(64, [3, 3], activation='relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.Dropout(0.25))
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128, activation='relu'))
model.add(tf.keras.layers.Dropout(0.5))
model.add(tf.keras.layers.Dense(1))

model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              optimizer=tf.keras.optimizers.Adam(),
              metrics=['accuracy'])

model.summary()

# Load data
x_train = np.load('xtrain_filtered.npy')
y_train = np.load('ytrain_filtered.npy')

x_test = np.load('xtest_filtered.npy')
y_test = np.load('ytest_filtered.npy')

log_dir = "/home/jovyan/tb_logs/"
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

history = model.fit(x_train, y_train, batch_size=128, epochs=100, verbose=1, validation_data=(x_test, y_test), callbacks=[tensorboard_callback])
cnn_results = model.evaluate(x_test, y_test)
