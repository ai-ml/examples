In Jupyter notebook terminal, run:

### Create a Pipeline
`kfp pipeline upload -p mnist-pipeline mnist-pipelineyaml`

### Run a Pipeline
`kfp run submit -e experiment-name -n mnist-pipeline -w -r first-run`
