# Remote Pipelines

## What is it about
Submit a pipeline run to ml.cern.ch from remote.

## How to run

### Obtain an authentication cookie
Two ways:
- Manually, via browser
- CERN SSO tool, run from lxplus - https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie/-/tree/master

### Edit remote_kfp.py to add the obtained cookie
- Edit line with `cookies='authservice_session=...'`

### Edit remote_kfp.py to use personal namespace
- Edit line with `namespace = 'dgolubov'`

### Install kfp library in your Python environment
- `pip install kfp`

### Run remote_kfp.py from lxplus or a CERN proxied machine
```
python3 remote_kfp.py
```
