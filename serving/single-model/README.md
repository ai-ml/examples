### Create InferenceService

```kubectl apply -f flowers.yaml```

### Run Inference

#### Internal, no authentication

- `NAMESPACE` is a personal Kubeflow namespace, which can be seen in the top left corner of the UI

```
curl http://flower-sample.NAMESPACE.svc.cluster.local/v1/models/flower-sample:predict -d @./input.json
```

##### Test Serverless

- Open two Terminals in the notebook server side by side
- In one Terminal watch the number of pods with:
```
watch -n 1 'kubectl get po | grep flower-sample'
```

- In the other Terminal submit multiple curl requests with:
```
for i in {1..1000}; do curl http://flower-sample.NAMESPACE.svc.cluster.local/v1/models/flower-sample:predict -d @./input.json & done
```

You should see number of serving pods in the first Terminal scaling according to the number of requests.

#### External, authentication

##### Obtain an Authentication Session Cookie via Chrome

- Click `View -> Developer Tools -> Network`
- Navigate to [ml.cern.ch](https://ml.cern.ch)
- Check Request Headers
    - Copy section `authservice_session` to the file `cookie`

```
curl -H "Host: flower-sample.NAMESPACE.example.com" -H @cookie https://ml.cern.ch/v1/models/flower-sample:predict -d @./input.json
```

